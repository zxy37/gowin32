package types

type (
	WORD       uint16
	DWORD      uint32
	PULONGLONG uint64
)
