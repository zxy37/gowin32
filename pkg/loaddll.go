package pkg

import "syscall"

var IsSystemDLL = map[string]bool{}

// Add notes that dll is a system32 DLL which should only be loaded
// from the Windows SYSTEM32 directory. It returns its argument back,
// for ease of use in generated code.
func Add(dll string) string {
	IsSystemDLL[dll] = true
	return dll
}

var (
	ModAdvapi32 = syscall.NewLazyDLL(Add("advapi32.dll"))
	ModCrypt32  = syscall.NewLazyDLL(Add("crypt32.dll"))
	ModDnsapi   = syscall.NewLazyDLL(Add("dnsapi.dll"))
	ModIphlpapi = syscall.NewLazyDLL(Add("iphlpapi.dll"))
	ModKernel32 = syscall.NewLazyDLL(Add("kernel32.dll"))
	ModMswsock  = syscall.NewLazyDLL(Add("mswsock.dll"))
	ModNetapi32 = syscall.NewLazyDLL(Add("netapi32.dll"))
	ModNtdll    = syscall.NewLazyDLL(Add("ntdll.dll"))
	ModSecur32  = syscall.NewLazyDLL(Add("secur32.dll"))
	ModShell32  = syscall.NewLazyDLL(Add("shell32.dll"))
	ModUserenv  = syscall.NewLazyDLL(Add("userenv.dll"))
	ModWs2_32   = syscall.NewLazyDLL(Add("ws2_32.dll"))
)
